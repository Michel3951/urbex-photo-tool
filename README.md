![Example](https://i.imgur.com/0U87iSw.jpg)

# About the app
This app is used to seperate my (best) urbexing photos. After I go urbexing I put all the photos in their corresponding directory. Then I check each directory for the best photos. The problem is that I shoot photos in JPG and RAW format, so I need to have both of these file types. This script allows me to select the photos I like, and seperate their types in to different folders so I don't have to spend countless hours copying the photos manually. The script can take some time to run because of file sizes (JPG avg: 8mb, CR2 avg: 30mb).

# Installing and running the app

> Requires node.js & typescript to be installed on your system

Install typescript globally:
```sh
npm i -g typescript
```

Building the app:
```sh
npm run build
```

Running the app:
```sh
node .
```

# Example
In this example there are very few photos, my average adventure has over 300+ photos so it gets messy very fast, but you get the point.

Example directory structure:

```
Urbexing
└───05-06-2022
    └───Leopard Tank
        |   IMG_001.JPG
        |   IMG_001.CR2
        |   IMG_002.JPG
        |   IMG_002.CR2
        |   IMG_003.JPG
        |   IMG_003.CR2
        |   IMG_004.JPG
        |   IMG_004.CR2
        |   IMG_005.JPG
        |   IMG_005.CR2
        |   IMG_006.JPG
        |   IMG_006.CR2
        |   IMG_007.JPG
        |   IMG_007.CR2
        |
        └───Best
            |   IMG_003.JPG
            |   IMG_007.JPG
```

Directory structure after running the script:
```
Urbexing
└───05-06-2022
    └───Leopard Tank
        |   IMG_001.JPG
        |   IMG_001.CR2
        |   IMG_002.JPG
        |   IMG_002.CR2
        |   IMG_003.JPG
        |   IMG_003.CR2
        |   IMG_004.JPG
        |   IMG_004.CR2
        |   IMG_005.JPG
        |   IMG_005.CR2
        |   IMG_006.JPG
        |   IMG_006.CR2
        |   IMG_007.JPG
        |   IMG_007.CR2
        |
        └───Best
        |   |   IMG_003.JPG
        |   |   IMG_007.JPG
        |
        └───CR2
        |   |   IMG_003.CR2
        |   |   IMG_007.CR2
        |
        └───JPG
            |   IMG_003.JPG
            |   IMG_007.JPG
```
## Real life example after using the app

![Current Folder](https://i.imgur.com/XeyrDG7.jpg)