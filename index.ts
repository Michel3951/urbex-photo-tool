import {
    Terminal
} from "terminal-kit"
import * as fs from 'fs'
import {
    ProgressBarController,
    ProgressBarOptions
} from "terminal-kit/Terminal"

const term: Terminal = require('terminal-kit').terminal

async function start(): Promise < void > {
    const exclude: string[] = ['./', '../', 'app', 'Best']

    // List all the main directories
    const parentDirectories: string[] = fs.readdirSync(__dirname + '/../')
        .filter(
            x => !exclude.includes(x)
        )

    term.yellow('Directories: ')
    .white(
        parentDirectories.join(', ')
    )

    term.yellow("\nSelect directory (type 'exit' to quit): ")

    term.inputField({
            autoComplete: parentDirectories,
            autoCompleteHint: true
        },
        function (error, directory: string) {
            if (directory === 'exit') process.exit(0)

            // If the given directory does not exist, ask them to re-enter
            if (!parentDirectories.includes(directory)) {
                term.red('\nInvalid directory, try again.')
                this(error, directory)
                return
            }

            term.green("\nDirectory selected.")

            // Retrieve all subdirectories in the selected directory
            const subDirectories: string[] = fs.readdirSync(__dirname + '/../' + directory + '/').filter(x => !exclude.includes(x))

            term.yellow('\nSub directories: ')
                .white(
                    subDirectories.join(', ')
                )

            term.yellow("\nSelect sub directory: ")

            term.inputField({
                    autoComplete: subDirectories,
                    autoCompleteHint: true
                },

                function (error, subDirectory) {
                    // Check if subdirectory exists, otherwise re-enter
                    if (!subDirectories.includes(subDirectory)) {
                        term.red('\nInvalid directory, try again.')
                        this(error, subDirectory)
                        return
                    }

                    term.green("\nSelected directory: ").white('Urbex/%s/%s', directory, subDirectory)

                    // The path to the folder containing the best images
                    const bestPath: string = __dirname + '/../' + directory + '/' + subDirectory + '/Best'
                    // The path to the folder containing all images
                    const path: string = __dirname + '/../' + directory + '/' + subDirectory

                    if (!fs.existsSync(bestPath)) {
                        term.bgBrightRed
                            .white('\nDirectory does not contain Best folder, aborting.')
                        process.exit(0)
                    }

                    const files: string[] = fs.readdirSync(bestPath).filter(x => !exclude.includes(x))

                    // Only look for these file types (JPG + RAW)
                    const fileTypes: string[] = ['JPG', 'CR2']

                    term.green('\nSeperating file types: ')
                        .white(
                            Array.from(fileTypes).join(', ')
                        )

                    // Create a progressbar so the user knows what is going on
                    const progressBar: ProgressBarController = term.progressBar( < ProgressBarOptions > {
                        width: 80,
                        title: 'Copying files',
                        eta: true,
                        percent: true,
                        items: files.length,
                        syncMode: true
                    })

                    // Loop over all file types and check if a directory exists for the given type, if not create it
                    for (let type of fileTypes) {
                        if (!fs.existsSync(path + '/' + type)) {
                            term.yellow('\nDirectory %s does not exist, creating it now.', type)
                            fs.mkdirSync(path + '/' + type + '/')
                        }
                    }

                    files.forEach((file: string) => {
                        progressBar.startItem(file)

                        const [filename, ext]: string[] = file.split('.')
                        
                        if (!fileTypes.includes(ext)) {
                            return
                        }

                        // Copy the files to their respective directory
                        if (ext === 'CR2') {
                            fs.copyFileSync(path + '/' + file, path + '/' + ext + '/' + file)
                            fs.copyFileSync(path + '/' + filename + '.JPG', path + '/' + 'JPG' + '/' + filename + '.JPG')
                        } else {
                            fs.copyFileSync(path + '/' + file, path + '/' + ext + '/' + file)

                            // I don't always shoot in JPG + RAW, so there aren't RAW files all the time
                            if (fs.existsSync(path + '/' + filename + '.CR2')) {
                                fs.copyFileSync(path + '/' + filename + '.CR2', path + '/' + 'CR2' + '/' + filename + '.CR2')
                            }
                        }

                        progressBar.itemDone(file)
                    })

                    term.brightGreen('\nFiles seperated :)\n')
                    // Ask if they want to convert another directory
                    start()
                })
        }
    )
}

// Initialize the application
start()